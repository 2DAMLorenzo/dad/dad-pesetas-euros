﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConversorEuroPeseta
{
    class Converter
    {
        private const double PESETA_VALUE = 166;
        private double number;

        public Converter()
        {
            this.number = 0.0d;
        }

        public void addNum(int num)
        {
            this.number = double.Parse(number.ToString()+ num.ToString());
        }

        public double getResult()
        {
            return number;
        }

        public void euroToPeseta()
        {
            number = number * PESETA_VALUE;
        }

        public void pesetaToEuro()
        {
            number = number / PESETA_VALUE;
        }
    }
}
