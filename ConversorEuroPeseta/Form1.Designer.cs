﻿namespace ConversorEuroPeseta
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button12 = new System.Windows.Forms.Button();
            this.Button11 = new System.Windows.Forms.Button();
            this.resultBox = new System.Windows.Forms.TextBox();
            this.Button8 = new System.Windows.Forms.Button();
            this.Button9 = new System.Windows.Forms.Button();
            this.Button10 = new System.Windows.Forms.Button();
            this.Button6 = new System.Windows.Forms.Button();
            this.Button7 = new System.Windows.Forms.Button();
            this.Button5 = new System.Windows.Forms.Button();
            this.Button4 = new System.Windows.Forms.Button();
            this.Button3 = new System.Windows.Forms.Button();
            this.Button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Button12
            // 
            this.Button12.Location = new System.Drawing.Point(184, 172);
            this.Button12.Name = "Button12";
            this.Button12.Size = new System.Drawing.Size(88, 37);
            this.Button12.TabIndex = 27;
            this.Button12.Text = "pesetas - euros";
            this.Button12.UseVisualStyleBackColor = true;
            this.Button12.Click += new System.EventHandler(this.Button12_Click);
            // 
            // Button11
            // 
            this.Button11.Location = new System.Drawing.Point(184, 129);
            this.Button11.Name = "Button11";
            this.Button11.Size = new System.Drawing.Size(88, 37);
            this.Button11.TabIndex = 26;
            this.Button11.Text = "euros - pesetas";
            this.Button11.UseVisualStyleBackColor = true;
            this.Button11.Click += new System.EventHandler(this.Button11_Click);
            // 
            // resultBox
            // 
            this.resultBox.Location = new System.Drawing.Point(29, 50);
            this.resultBox.Name = "resultBox";
            this.resultBox.Size = new System.Drawing.Size(129, 20);
            this.resultBox.TabIndex = 25;
            this.resultBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Button8
            // 
            this.Button8.Location = new System.Drawing.Point(119, 86);
            this.Button8.Name = "Button8";
            this.Button8.Size = new System.Drawing.Size(39, 37);
            this.Button8.TabIndex = 24;
            this.Button8.Text = "9";
            this.Button8.UseVisualStyleBackColor = true;
            this.Button8.Click += new System.EventHandler(this.Button8_Click);
            // 
            // Button9
            // 
            this.Button9.Location = new System.Drawing.Point(74, 86);
            this.Button9.Name = "Button9";
            this.Button9.Size = new System.Drawing.Size(39, 37);
            this.Button9.TabIndex = 23;
            this.Button9.Text = "8";
            this.Button9.UseVisualStyleBackColor = true;
            this.Button9.Click += new System.EventHandler(this.Button9_Click);
            // 
            // Button10
            // 
            this.Button10.Location = new System.Drawing.Point(29, 86);
            this.Button10.Name = "Button10";
            this.Button10.Size = new System.Drawing.Size(39, 37);
            this.Button10.TabIndex = 22;
            this.Button10.Text = "7";
            this.Button10.UseVisualStyleBackColor = true;
            this.Button10.Click += new System.EventHandler(this.Button10_Click);
            // 
            // Button6
            // 
            this.Button6.Location = new System.Drawing.Point(119, 129);
            this.Button6.Name = "Button6";
            this.Button6.Size = new System.Drawing.Size(39, 37);
            this.Button6.TabIndex = 21;
            this.Button6.Text = "6";
            this.Button6.UseVisualStyleBackColor = true;
            this.Button6.Click += new System.EventHandler(this.Button6_Click);
            // 
            // Button7
            // 
            this.Button7.Location = new System.Drawing.Point(74, 129);
            this.Button7.Name = "Button7";
            this.Button7.Size = new System.Drawing.Size(39, 37);
            this.Button7.TabIndex = 20;
            this.Button7.Text = "5";
            this.Button7.UseVisualStyleBackColor = true;
            this.Button7.Click += new System.EventHandler(this.Button7_Click);
            // 
            // Button5
            // 
            this.Button5.Location = new System.Drawing.Point(29, 129);
            this.Button5.Name = "Button5";
            this.Button5.Size = new System.Drawing.Size(39, 37);
            this.Button5.TabIndex = 19;
            this.Button5.Text = "4";
            this.Button5.UseVisualStyleBackColor = true;
            this.Button5.Click += new System.EventHandler(this.Button5_Click);
            // 
            // Button4
            // 
            this.Button4.Location = new System.Drawing.Point(29, 215);
            this.Button4.Name = "Button4";
            this.Button4.Size = new System.Drawing.Size(129, 37);
            this.Button4.TabIndex = 18;
            this.Button4.Text = "0";
            this.Button4.UseVisualStyleBackColor = true;
            this.Button4.Click += new System.EventHandler(this.Button4_Click);
            // 
            // Button3
            // 
            this.Button3.Location = new System.Drawing.Point(119, 172);
            this.Button3.Name = "Button3";
            this.Button3.Size = new System.Drawing.Size(39, 37);
            this.Button3.TabIndex = 17;
            this.Button3.Text = "3";
            this.Button3.UseVisualStyleBackColor = true;
            this.Button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // Button2
            // 
            this.Button2.Location = new System.Drawing.Point(74, 172);
            this.Button2.Name = "Button2";
            this.Button2.Size = new System.Drawing.Size(39, 37);
            this.Button2.TabIndex = 16;
            this.Button2.Text = "2";
            this.Button2.UseVisualStyleBackColor = true;
            this.Button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Conversor";
            // 
            // Button1
            // 
            this.Button1.Location = new System.Drawing.Point(29, 172);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(39, 37);
            this.Button1.TabIndex = 14;
            this.Button1.Text = "1";
            this.Button1.UseVisualStyleBackColor = true;
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 276);
            this.Controls.Add(this.Button12);
            this.Controls.Add(this.Button11);
            this.Controls.Add(this.resultBox);
            this.Controls.Add(this.Button8);
            this.Controls.Add(this.Button9);
            this.Controls.Add(this.Button10);
            this.Controls.Add(this.Button6);
            this.Controls.Add(this.Button7);
            this.Controls.Add(this.Button5);
            this.Controls.Add(this.Button4);
            this.Controls.Add(this.Button3);
            this.Controls.Add(this.Button2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button Button12;
        internal System.Windows.Forms.Button Button11;
        internal System.Windows.Forms.TextBox resultBox;
        internal System.Windows.Forms.Button Button8;
        internal System.Windows.Forms.Button Button9;
        internal System.Windows.Forms.Button Button10;
        internal System.Windows.Forms.Button Button6;
        internal System.Windows.Forms.Button Button7;
        internal System.Windows.Forms.Button Button5;
        internal System.Windows.Forms.Button Button4;
        internal System.Windows.Forms.Button Button3;
        internal System.Windows.Forms.Button Button2;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Button Button1;
    }
}

