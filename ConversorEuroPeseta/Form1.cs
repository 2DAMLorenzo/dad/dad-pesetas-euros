﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ConversorEuroPeseta
{
    public partial class Form1 : Form
    {
        private Converter converter;
        public Form1()
        {
            converter = new Converter();
            InitializeComponent();
            resultBox.Text = converter.getResult().ToString();
        }

        private void updateDisplay()
        {
            resultBox.Text = converter.getResult().ToString();
        }

        private void setNumber(int num)
        {
            converter.addNum(num);
            updateDisplay();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            setNumber(1);
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            setNumber(0);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            setNumber(2);
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            setNumber(3);
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            setNumber(4);
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            setNumber(5);
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            setNumber(6);
        }

        private void Button10_Click(object sender, EventArgs e)
        {
            setNumber(7);
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            setNumber(8);
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            setNumber(9);
        }

        private void Button11_Click(object sender, EventArgs e)
        {
            converter.euroToPeseta();
            updateDisplay();
        }

        private void Button12_Click(object sender, EventArgs e)
        {
            converter.pesetaToEuro();
            updateDisplay();
        }
    }
}
